module.exports = function(app) {

    var validaUsuario = function(req, res) {
        res.format({
			html: function(){
				res.render('usuarios/lista');					
			},
			json: function(){
                var connection = app.infra.connectionFactory();
                var usuariosDAO = new app.infra.UsuariosDAO(connection);
                
                var usuario = req.body;
                var jsonRetorno = {};

                if (usuario.email){
                    if (usuario.senha){
                        usuariosDAO.validaUsuarioJson(usuario.email, usuario.senha, function(err, results) {
                            jsonRetorno = {
                                'usuario' : results
                            }
                            res.json(jsonRetorno)
                        });                        
                    } else {
                        jsonRetorno = {					
                            'mensagem' : 'Senha inválida!'
                        }				
                        res.json(jsonRetorno);
                    }
                } else {
                    jsonRetorno = {
                        'mensagem' : 'Usuário não cadastrado!'
                    }
                    res.json(jsonRetorno);                   
                }

                connection.end();
			}
		});
    }

    app.get('/usuarios/login',function(req, res) {
        res.send('Ok.');
        // res.render('usuarios/lista');
    });

	app.post('/usuarios/login',validaUsuario);

}