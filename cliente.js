var http = require('http');

var configuracoes = {
	hostname: 'localhost',
	port:8080,
	path: '/usuarios/login',
	method: 'post',
	headers: {
		'Accept': 'application/json',
		'Content-type': 'application/json' 
	}
};

var client = http.request(configuracoes, function(res) {
	console.log(res.statusCode);
	res.on('data', function(body) {
		console.log('Corpo: ' + body);		
	});
});

var usuario = {
	email : 'lucas.limeres@gmail.com',
	senha : '123'
};

client.end(JSON.stringify(usuario));